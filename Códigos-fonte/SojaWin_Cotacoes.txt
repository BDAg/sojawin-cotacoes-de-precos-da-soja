#SojaWin_Cotacoes.py


#Versao v4 (Final)

import requests
from lxml import html

import csv


print()

#Retorna numero de linha do Banco de Dados
def Num_Linhas_BD(Local_arquivo):
    Num_Linha = 0
    with open(Local_arquivo, 'r+', newline='') as csv_file:
        Num_Linha = sum(1 for line in csv_file)     # conta a legenda
    return (Num_Linha)

#Fun��o para verificar se o novo dado est� no Banco de Dados
def Verifica_NewData_BD(Local_arquivo, Dado_Ver):
    with open(Local_arquivo, 'r') as csv_file:   
        arquivo = csv.reader(csv_file, delimiter=';')
        Num_Linha_QueNaoTem = 0
        for i in arquivo:
            if (i[0] == Dado_Ver):
                break  
            else:
                Num_Linha_QueNaoTem += 1
    return (Num_Linha_QueNaoTem)


#Fun��o para Criar um Banco de Dados .csv do CEPEA ESALQ
def Cria_BD_CEPEA_ESALQ(Local_arquivo, Site):
    pagina = requests.get (Site)
    dados = html.fromstring(pagina.content)

    #indicador = 1 (PARANAGUA) e 2 (PARANA)
    Paranagua = 1
    Parana = 2   

    with open(Local_arquivo, 'w', newline='') as arquivo:
        final = csv.writer(arquivo, delimiter=';')
        #final = csv.writer(arquivo, dialect='excel')
        final.writerow(["DATA", "CIDADE", "VALOR R$", "VAR./DIA", "VAR./M�S", "VALOR US$"])
        
        
        for cont in range(15, 0, -1):   #OBS:15 � o n�mero de dados que ficam disponiveis no site
            #para PARANAGUA
            dados_linha = dados.xpath('//*[@id="imagenet-indicador{}"]/tbody/tr[{}]/td/text()'.format(Paranagua, cont))
            
            #Adiciona a Cidade juntos aos dados coletados
            dados_c_City = []
            for i in range(6):
                if (i == 0):
                    dados_c_City.append(dados_linha[i])
                    
                elif (i == 1):
                    dados_c_City.append('Paranagu�')
                else:
                    dados_c_City.append(dados_linha[i-1])

            final.writerow(dados_c_City)
            
            #para PARANA
            dados_linha = dados.xpath('//*[@id="imagenet-indicador{}"]/tbody/tr[{}]/td/text()'.format(Parana, cont))
            
            #Adiciona a Cidade juntos aos dados coletados
            dados_c_City = []
            for i in range(6):
                if (i == 0):
                    dados_c_City.append(dados_linha[i])
                    
                elif (i == 1):
                    dados_c_City.append('Paran�')
                else:
                    dados_c_City.append(dados_linha[i-1])

            final.writerow(dados_c_City)

    print('Arquivos criados com sucesso!')

#Fun��o para Criar um Banco de Dados .csv do CANAL RURAL
def Cria_BD_CANAL_RURAL(Local_arquivo, Site):
    pagina = requests.get (Site)
    dados = html.fromstring(pagina.content)
 

    with open(Local_arquivo, 'w', newline='') as arquivo:
        final = csv.writer(arquivo, delimiter=';')
        #final = csv.writer(arquivo, dialect='excel')
        final.writerow(["DATA", "CIDADE", "VALOR R$", "VAR./DIA", "VAR./M�S", "VALOR US$"])

                
        for cont in range(1, 9):    #for de 8 pois sao o numero de cidades
            dados_linha = ['-'] * 6
            #Data
            dados_linha[0] = dados.xpath('//*[@id="mobile"]/div/div[2]/div/div/div/div/div/div/div[1]/div[4]/div/div[2]/div/div/div/div/div/div/div[2]/div/div/p/span/b/text()')[0]
            #Cidade
            dados_linha[1] = dados.xpath('//*[@id="desktop"]/div/div/div[1]/div[5]/div/div[2]/div/div/div/div/div/div/div[2]/div/table[1]/tbody/tr[{}]/td[1]/text()'.format(cont))[0]
            #Pre�o
            dados_linha[2] = dados.xpath('//*[@id="mobile"]/div/div[2]/div/div/div/div/div/div/div[1]/div[4]/div/div[2]/div/div/div/div/div/div/div[2]/div/table[1]/tbody/tr[{}]/td[2]/text()'.format(cont))[0]

            final.writerow(dados_linha)
    print('Arquivos criados com sucesso!')
    
#Fun��o para Criar um Banco de Dados .csv do noticias AGRICOLAS
def Cria_BD_noticias_AGRICOLAS(Local_arquivo, Site):
    pagina = requests.get (Site)
    dados = html.fromstring(pagina.content)
 

    with open(Local_arquivo, 'w', newline='') as arquivo:
        final = csv.writer(arquivo, delimiter=';')
        #final = csv.writer(arquivo, dialect='excel')
        final.writerow(["DATA", "CIDADE", "VALOR R$", "VAR./DIA", "VAR./M�S", "VALOR US$"])

                
        for cont in range(1, 40):    #for de 39 pois sao o numero de cidades
            dados_linha = ['-'] * 6

            #Data
            Data = dados.xpath('//*[@id="content"]/div[3]/div[3]/div[1]/div[1]/div/text()')[0]
            #converte string em lista
            Data = list(Data)
            del(Data[0:12])
            #converte lista em string
            dados_linha[0] = ''.join(Data)

            #Cidades
            Cidade = dados.xpath('//*[@id="content"]/div[3]/div[3]/div[1]/div[2]/table/tbody/tr[{}]/td[1]/text()'.format(cont))[0]
            
            #Para deixar apenas o nome da cidade
            #converte string em lista
            Cidade = list(Cidade)
            for i in range(len(Cidade)):
                if (Cidade[i] == '('):
                    del(Cidade[i-1:len(Cidade)])
                    break

            #Para corrigir os erros de acentos do nome da cidade      
            letra = 0
            cont_aux = len(Cidade)
            while (letra < (cont_aux-1)):

                if (Cidade[letra] == '�'):
                    if (Cidade[letra+1] == '�'):
                        Cidade[letra] = '�'
                        del(Cidade[letra+1])
                    elif (Cidade[letra+1] =='�'):
                        Cidade[letra] = '�'
                        del(Cidade[letra+1])
                    elif (Cidade[letra+1] =='�'):
                        Cidade[letra] = '�'
                        del(Cidade[letra+1])
                    elif (Cidade[letra+1] =='�'):
                        Cidade[letra] = '�'
                        del(Cidade[letra+1])
                    elif (Cidade[letra+1] =='�'):
                        Cidade[letra] = '�'
                        del(Cidade[letra+1])
                    elif (Cidade[letra+1] =='�'):
                        Cidade[letra] = '�'
                        del(Cidade[letra+1])
                    else:
                        Cidade[letra] = '�'
                        del(Cidade[letra+1])
                    cont_aux -= 1
                letra += 1

            if (Cidade[len(Cidade)-1] == ' '):
                del(Cidade[len(Cidade)-1])

            #converte lista em string
            Cidade = ''.join(Cidade)   
        
            dados_linha[1] = Cidade

            #Pre�o 
            Preco = dados.xpath('//*[@id="content"]/div[3]/div[3]/div[1]/div[2]/table/tbody/tr[{}]/td[2]/text()'.format(cont))[0]
            if (Preco[0] == 's'):
                Preco = 'sem cota��o'
            dados_linha[2] = Preco
            #Varia��o
            dados_linha[3] = dados.xpath('//*[@id="content"]/div[3]/div[3]/div[1]/div[2]/table/tbody/tr[{}]/td[3]/text()'.format(cont))[0]

            final.writerow(dados_linha)
    print('Arquivos criados com sucesso!')

#Fun��o para Adicionar os novos dados no Banco de Dados .csv do CEPEA ESALQ
def Adciona_BD_CEPEA_ESALQ(Local_arquivo, Site):
    pagina = requests.get (Site)
    dados = html.fromstring(pagina.content)

    #indicador = 1 (PARANAGUA) e 2 (PARANA)
    Paranagua = 1
    Parana = 2

    Num_Linhas_Arq = Num_Linhas_BD(Local_arquivo)
    print('numero linhas antes de atualizar:', Num_Linhas_Arq)

    with open(Local_arquivo, 'a', newline='') as arquivo:
        final = csv.writer(arquivo, delimiter=';')
       
        for cont in range(15, 0, -1):   #OBS:15 � o n�mero de dados que ficam disponiveis no site
            #para PARANAGUA
            dados_linha = dados.xpath('//*[@id="imagenet-indicador{}"]/tbody/tr[{}]/td/text()'.format(Paranagua, cont))
            Num_Linha_QueNaoTem = Verifica_NewData_BD(Local_arquivo, dados_linha[0])

            if (Num_Linhas_Arq == Num_Linha_QueNaoTem):            
                #Adiciona a Cidade juntos aos dados coletados
                dados_c_City = []
                for i in range(6):
                    if (i == 0):
                        dados_c_City.append(dados_linha[i])
                    
                    elif (i == 1):
                        dados_c_City.append('Paranagu�')
                    else:
                        dados_c_City.append(dados_linha[i-1])

                final.writerow(dados_c_City)
            #para PARANA
            dados_linha = dados.xpath('//*[@id="imagenet-indicador{}"]/tbody/tr[{}]/td/text()'.format(Parana, cont))
            Num_Linha_QueNaoTem = Verifica_NewData_BD(Local_arquivo, dados_linha[0])

            if (Num_Linhas_Arq == Num_Linha_QueNaoTem):            
                #Adiciona a Cidade juntos aos dados coletados
                dados_c_City = []
                for i in range(6):
                    if (i == 0):
                        dados_c_City.append(dados_linha[i])
                    
                    elif (i == 1):
                        dados_c_City.append('Paran�')
                    else:
                        dados_c_City.append(dados_linha[i-1])

                final.writerow(dados_c_City)

    print('Arquivos atualizados com sucesso!')
    Num_Linhas_Arq = Num_Linhas_BD(Local_arquivo)
    print('numero linhas depois de atualizar:', Num_Linhas_Arq)

#Fun��o para Adcionar um Banco de Dados .csv do CANAL RURAL
def Adciona_BD_CANAL_RURAL(Local_arquivo, Site):
    pagina = requests.get (Site)
    dados = html.fromstring(pagina.content)

    Num_Linhas_Arq = Num_Linhas_BD(Local_arquivo)
    print('numero linhas antes de atualizar:', Num_Linhas_Arq) 

    with open(Local_arquivo, 'a', newline='') as arquivo:
        final = csv.writer(arquivo, delimiter=';')
                
        for cont in range(1, 9):    #for de 8 pois sao o numero de cidades
            dados_linha = ['-'] * 6
            #Data
            dados_linha[0] = dados.xpath('//*[@id="mobile"]/div/div[2]/div/div/div/div/div/div/div[1]/div[4]/div/div[2]/div/div/div/div/div/div/div[2]/div/div/p/span/b/text()')[0]
            Num_Linha_QueNaoTem = Verifica_NewData_BD(Local_arquivo, dados_linha[0])
            
            #Cidade
            dados_linha[1] = dados.xpath('//*[@id="desktop"]/div/div/div[1]/div[5]/div/div[2]/div/div/div/div/div/div/div[2]/div/table[1]/tbody/tr[{}]/td[1]/text()'.format(cont))[0]
            #Pre�o
            dados_linha[2] = dados.xpath('//*[@id="mobile"]/div/div[2]/div/div/div/div/div/div/div[1]/div[4]/div/div[2]/div/div/div/div/div/div/div[2]/div/table[1]/tbody/tr[{}]/td[2]/text()'.format(cont))[0]
            
            if (Num_Linhas_Arq == Num_Linha_QueNaoTem):
                final.writerow(dados_linha)
    print('Arquivos criados com sucesso!')
    Num_Linhas_Arq = Num_Linhas_BD(Local_arquivo)
    print('numero linhas depois de atualizar:', Num_Linhas_Arq)

#Fun��o para Adcionar um Banco de Dados .csv do noticias AGRICOLAS
def Adciona_BD_noticias_AGRICOLAS(Local_arquivo, Site):
    pagina = requests.get (Site)
    dados = html.fromstring(pagina.content)

    Num_Linhas_Arq = Num_Linhas_BD(Local_arquivo)
    print('numero linhas antes de atualizar:', Num_Linhas_Arq) 

    with open(Local_arquivo, 'a', newline='') as arquivo:
        final = csv.writer(arquivo, delimiter=';')
                
        for cont in range(1, 40):    #for de 39 pois sao o numero de cidades
            dados_linha = ['-'] * 6

            #Data
            Data = dados.xpath('//*[@id="content"]/div[3]/div[3]/div[1]/div[1]/div/text()')[0]
            #converte string em lista
            Data = list(Data)
            del(Data[0:12])
            #converte lista em string
            dados_linha[0] = ''.join(Data)
            Num_Linha_QueNaoTem = Verifica_NewData_BD(Local_arquivo, dados_linha[0])

            #Cidades
            Cidade = dados.xpath('//*[@id="content"]/div[3]/div[3]/div[1]/div[2]/table/tbody/tr[{}]/td[1]/text()'.format(cont))[0]
            
            #Para deixar apenas o nome da cidade
            #converte string em lista
            Cidade = list(Cidade)
            for i in range(len(Cidade)):
                if (Cidade[i] == '('):
                    del(Cidade[i-1:len(Cidade)])
                    break

            #Para corrigir os erros de acentos do nome da cidade      
            letra = 0
            cont_aux = len(Cidade)
            while (letra < (cont_aux-1)):

                if (Cidade[letra] == '�'):
                    if (Cidade[letra+1] == '�'):
                        Cidade[letra] = '�'
                        del(Cidade[letra+1])
                    elif (Cidade[letra+1] =='�'):
                        Cidade[letra] = '�'
                        del(Cidade[letra+1])
                    elif (Cidade[letra+1] =='�'):
                        Cidade[letra] = '�'
                        del(Cidade[letra+1])
                    elif (Cidade[letra+1] =='�'):
                        Cidade[letra] = '�'
                        del(Cidade[letra+1])
                    elif (Cidade[letra+1] =='�'):
                        Cidade[letra] = '�'
                        del(Cidade[letra+1])
                    elif (Cidade[letra+1] =='�'):
                        Cidade[letra] = '�'
                        del(Cidade[letra+1])
                    else:
                        Cidade[letra] = '�'
                        del(Cidade[letra+1])
                    cont_aux -= 1
                letra += 1

            if (Cidade[len(Cidade)-1] == ' '):
                del(Cidade[len(Cidade)-1])

            #converte lista em string
            Cidade = ''.join(Cidade)   
        
            dados_linha[1] = Cidade

            #Pre�o 
            Preco = dados.xpath('//*[@id="content"]/div[3]/div[3]/div[1]/div[2]/table/tbody/tr[{}]/td[2]/text()'.format(cont))[0]
            if (Preco[0] == 's'):
                Preco = 'sem cota��o'
            dados_linha[2] = Preco
            #Varia��o
            dados_linha[3] = dados.xpath('//*[@id="content"]/div[3]/div[3]/div[1]/div[2]/table/tbody/tr[{}]/td[3]/text()'.format(cont))[0]

            if (Num_Linhas_Arq == Num_Linha_QueNaoTem):
                final.writerow(dados_linha)
    print('Arquivos criados com sucesso!')
    Num_Linhas_Arq = Num_Linhas_BD(Local_arquivo)
    print('numero linhas depois de atualizar:', Num_Linhas_Arq)

#Imprime Banco de Dados
def Print_BD(Local_arquivo):
    with open(Local_arquivo, 'r') as csv_file:  
        arquivo = csv.reader(csv_file, delimiter=';')
        for i in arquivo:
            print(str(i[0]).rjust(11, ' '), str(i[1]).rjust(34, ' '), str(i[2]).rjust(14, ' '), str(i[3]).rjust(10, ' '), str(i[4]).rjust(10, ' '), str(i[5]).rjust(12, ' '))

#CEPEA ESALQ
Site_1 = 'https://www.cepea.esalq.usp.br/br/indicador/soja.aspx'
BD_CEPEA_Esalq = '/content/drive/MyDrive/SojaWin_BDAg_(WebBot)/cepea_esalq_Soja.csv'
#CANAL RURAL
Site_2 = 'https://www.canalrural.com.br/cotacao/soja/'
BD_Canal_Rural = '/content/drive/MyDrive/SojaWin_BDAg_(WebBot)/canal_rural_Soja.csv'
#noticias AGRICOLAS
Site_3 = 'https://www.noticiasagricolas.com.br/cotacoes/soja/soja-mercado-fisico-sindicatos-e-cooperativas'
BD_noticias_AGRICOLAS = '/content/drive/MyDrive/SojaWin_BDAg_(WebBot)/noticias_agricolas_Soja.csv'

#Para Criar
#print('CEPEA_Esalq')
#Cria_BD_CEPEA_ESALQ('cepea_esalq_Soja.csv', Site_1)
#Print_BD(BD_CEPEA_Esalq)
#print('CANAL_RURAL')
#Cria_BD_CANAL_RURAL('canal_rural_Soja.csv', Site_2)
#Print_BD(BD_Canal_Rural)
#print('noticias_AGRICOLAS')
#Cria_BD_noticias_AGRICOLAS('noticias_agricolas_Soja.csv', Site_3)
#Print_BD(BD_noticias_AGRICOLAS)

#Para Adcionar
print('CEPEA_Esalq')
Adciona_BD_CEPEA_ESALQ(BD_CEPEA_Esalq, Site_1)
#Print_BD(BD_CEPEA_Esalq)
print('CANAL_RURAL')
Adciona_BD_CANAL_RURAL(BD_Canal_Rural, Site_2)
#Print_BD(BD_Canal_Rural)
print('noticias_AGRICOLAS')
Adciona_BD_noticias_AGRICOLAS(BD_noticias_AGRICOLAS, Site_3)
#Print_BD(BD_noticias_AGRICOLAS)
